<?php

use App\Http\Controllers\Website\AgendaController;
use App\Http\Controllers\Website\ClientesController;
use App\Http\Controllers\Website\DashboardController;
use App\Http\Controllers\Website\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('website.home.index');
});

Route::get('login', [LoginController::class, 'index'])->name('app.login');
Route::get('dashboard', [DashboardController::class, 'index'])->name('app.dashboard');
Route::get('agenda', [AgendaController::class, 'index'])->name('app.agenda');
Route::get('clientes', [ClientesController::class, 'index'])->name('app.dashboard');
